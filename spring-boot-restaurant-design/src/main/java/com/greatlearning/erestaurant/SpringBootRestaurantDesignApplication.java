package com.greatlearning.erestaurant;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SpringBootRestaurantDesignApplication {

	public static void main(String[] args) {
		SpringApplication.run(SpringBootRestaurantDesignApplication.class, args);
	}

}
