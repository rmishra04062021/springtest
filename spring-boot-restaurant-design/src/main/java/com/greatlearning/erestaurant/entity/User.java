package com.greatlearning.erestaurant.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Entity
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class User {

	@Id
	@Column
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Integer userid;
	
	@Column
	private String password;
	
	@Column
	private String fullname;
	
	@Column
	private int mobilenumber;
	
	@Column
	private String emailid;
	
	@Column
	private String role_description;
	
	
}
