package com.greatlearning.erestaurant.serviceImpl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.greatlearning.erestaurant.entity.User;
import com.greatlearning.erestaurant.repository.UserRepository;
import com.greatlearning.erestaurant.service.UserService;

@Service
public class UserServiceImp implements UserService {
	
	@Autowired
	UserRepository userRepository;

	@Override
	public List<User> getAllUsers() {
		return userRepository.findAll();
	}
	

}
