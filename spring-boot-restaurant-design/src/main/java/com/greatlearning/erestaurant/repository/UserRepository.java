package com.greatlearning.erestaurant.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.greatlearning.erestaurant.entity.User;

@Repository
public interface UserRepository extends JpaRepository<User, Integer> {

}
