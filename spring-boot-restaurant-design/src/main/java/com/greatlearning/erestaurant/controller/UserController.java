package com.greatlearning.erestaurant.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.greatlearning.erestaurant.entity.User;
//import com.greatlearning.erestaurant.service.UserService;
import com.greatlearning.erestaurant.serviceImpl.UserServiceImp;

import io.swagger.annotations.ApiOperation;

@RestController
@RequestMapping("/userService")
public class UserController {
	
	@Autowired
	UserServiceImp  userService;
	
	@ApiOperation(value = "Get All Users", notes = "This API allows us to fetch all Users")
	@GetMapping("/getAllUsers")
	public List<User> getAllUsers(){
		System.out.println("data count : "+userService.getAllUsers().size());
		return userService.getAllUsers();
	}

}
