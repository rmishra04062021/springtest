package com.greatlearning.erestaurant.service;

import java.util.List;

import com.greatlearning.erestaurant.entity.User;

public interface UserService {
	
	public List<User> getAllUsers();

}
